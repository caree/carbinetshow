
var appTagReadServerConfig = require('./appTagReadServerConfig');
readerPortNameMapList = appTagReadServerConfig.readerPortNameMapList();

//自定义协议 [flag, epc]
// like [5301, 3005FB63AC1F3841EC880468]
simpleTagReadPortNameList = appTagReadServerConfig.simpleTagReadPortNameList();

/**
 * Module dependencies.
 */
var colors = require('colors');

colors.setTheme({
  silly: 'rainbow',
  input: 'grey',
  verbose: 'cyan',
  prompt: 'grey',
  info: 'green',
  data: 'grey',
  help: 'cyan',
  warn: 'yellow',
  debug: 'blue',
  error: 'red'
});
var EventProxy = require('eventproxy');
ep = new EventProxy();
var express = require('express');
var routes = require('./routes');
// var user = require('./routes/user');
var http = require('http');
var path = require('path');
var wsServerTagRead = require('./routes/wsServerTagRead');

var app = express();

var server = wsServerTagRead.startWebSocketServer(app);
// all environments
app.set('port', process.env.PORT || 6004);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/wsTest', routes.wsTest);
app.get('/', routes.tagReadTest);

// app.get('/users', user.list);

server.listen(app.get('port'), function(){
  console.log(('Tag read server listening on port ' + app.get('port')).info);
});
