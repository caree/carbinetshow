

listeningPorts = require('./appTestConfig').listeningPorts();
//测试参数
// listeningPort = 5101;// UDP端口
// listeningPort2 = 5102;// UDP端口
/**
 * Module dependencies.
 */
var EventProxy = require('eventproxy');
ep = new EventProxy();

var colors = require('colors');

colors.setTheme({
  silly: 'rainbow',
  input: 'grey',
  verbose: 'cyan',
  prompt: 'grey',
  info: 'green',
  data: 'grey',
  help: 'cyan',
  warn: 'yellow',
  debug: 'blue',
  error: 'red'
});

var udpServer = require('./routes/TestTagLogic');
var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');
var wsServer = require('./routes/wsServerTest');

var app = express();

var server = wsServer.startWebSocketServer(app);
// all environments
app.set('port', process.env.PORT || 6003);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.tagTest);
app.get('/tagTest', routes.tagTest);
app.get('/hcDemo', routes.hcDemo);

server.listen(app.get('port'), function(){
  console.log(('appTest server listening on port ' + app.get('port')).info);
});
