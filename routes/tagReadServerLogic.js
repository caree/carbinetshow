
// {readerName:'r5000', port:5000, tagList:[], subscriber:[]}

var request = require('request');
var _ = require("underscore");
var timeFormater = require('./timeFormat').getCurrentTime;
var InventoryUnit = require('./InventoryLogicUnit');
var EventProxy = require('eventproxy');
var logicEP = new EventProxy();
// require('./simulator');
// var inventoryReaderPortList = [];
var serverList = [];

_.each(readerPortNameMapList, function(_map){
	console.log('reader port   <====>   Name'.info);
	console.log(('       '+_map.port+'            '+_map.readerName).info);
});

_.each(simpleTagReadPortNameList, function(_map){
	console.log('reader port   <====>   Name'.info);
	console.log(('       '+_map.port+'            '+_map.readerName).info);
});

console.log('********************************************************'.data);

exports.startUDPListening = function(){
	_.each(readerPortNameMapList, function(_readerPortNameMap){
		startNewInventoryUnit(
			{listeningPort: _readerPortNameMap.port
				, ep: ep
				, minTagReadedConfirmCount: _readerPortNameMap.minTagReadedConfirmCount 
				, maxTagReadInterval: _readerPortNameMap.maxTagReadInterval});

		serverList.push({readerName: _readerPortNameMap.readerName, port:_readerPortNameMap.port, tagList:[], subscriber:[]});
	});

	_.each(simpleTagReadPortNameList, function(_readerPortNameMap){
		startSimpleTagReadReceiver({
			listeningPort: _readerPortNameMap.port
			, ep: ep
			, minTagReadedConfirmCount: _readerPortNameMap.minTagReadedConfirmCount || 1
			, maxTagReadInterval: _readerPortNameMap.maxTagReadInterval || 5000
			, parserType: 'simpleReader'
		});
		serverList.push({readerName: _readerPortNameMap.readerName, port:_readerPortNameMap.port, tagList:[], subscriber:[]});
	});
};

exports.removeSubscriber = removeSubscriber;
exports.addSubscriber = addSubscriber;
exports.getSubscribedExistTags = getSubscribedExistTags;

//***********************************************



function removeSubscriber(_ws){
	_.each(serverList, function(_server){
	    var index = _server.subscriber.indexOf(_ws);
	    if(index >= 0){
		    _server.subscriber.splice(index, 1);
	    }
	});
};

function addSubscriber(_ws, _readerList){
	//将ws客户端添加到每个它所订阅的读写器订阅者列表中
	_.each(_readerList, function(_readerName){
		console.log(('subscriber readerName => ' + _readerName).data);
		console.dir(serverList);
		var server = _.find(serverList, function(_server){
			return _server.readerName == _readerName;
		});
		if(server != null){
			console.log(('readerName => ' + server.readerName).data);
			var ws = _.find(server.subscriber, function(_subscriber){
				return _subscriber == _ws;
			});
			if(ws == null){
				server.subscriber.push(_ws);
				console.log(('add subscriber => ' + _readerName).info);
			}
		}else{
			console.log(('Can not find server ' + _readerName).warn);
		}
	});
};

function getSubscribedExistTags(_ws){

	var server = _.find(serverList, function(_server){
		return _.contains(_server.subscriber, _ws);
	});
	if(server != null){
		return server.tagList;
	}
	return [];
};

function startNewInventoryUnit(_paras){
	var listeningPort = _paras.listeningPort || 10000;
	var ep = _paras.ep || null;
	ep.tail('tagAdded'+listeningPort, function(_tags){
		console.log(('port ' + listeningPort + ' new tag ').info);
		_.each(_tags, function(_tag){
			console.log('_tag => ');
			console.dir(_tag);
			broadcastTagInfo({epc: _tag.epc, antenaID: _tag.antenaID, Event: 'added'}, listeningPort);
			console.log(('new tag ' +_tag.epc).info);
		});
	});
	ep.tail('tagDeleted'+listeningPort, function(_tags){
		console.log(('port ' + listeningPort + ' delete tag ').info);
		_.each(_tags, function(_tag){
			broadcastTagInfo({epc: _tag.epc, antenaID: _tag.antenaID, Event: 'deleted'}, listeningPort);
			console.log(('delete tag ' +_tag.epc).info);
		});		
	});

	var inventoryUnit = new InventoryUnit(_paras);
	inventoryUnit.startServer();
}
function startSimpleTagReadReceiver(_paras){
	var listeningPort = _paras.listeningPort || 10000;
	var ep = _paras.ep || null;
	ep.tail('tagAdded'+listeningPort, function(_tags){
		console.log(('port ' + listeningPort + ' new tag ').info);
		_.each(_tags, function(_tag){
			console.log('_tag => '.data);
			console.dir(_tag);
			broadcastTagInfo({epc: _tag.epc, antenaID: _tag.antenaID, Event: 'added'}, listeningPort);
			console.log(('new tag ' +_tag.epc).info);
		});
	});
	ep.tail('tagDeleted'+listeningPort, function(_tags){
		console.log(('port ' + listeningPort + ' delete tag ').info);
		_.each(_tags, function(_tag){
			broadcastTagInfo({epc: _tag.epc, antenaID: _tag.antenaID, Event: 'deleted'}, listeningPort);
			console.log(('delete tag ' +_tag.epc).info);
		});		
	});
	var inventoryUnit = new InventoryUnit(_paras);
	inventoryUnit.startServer();
}
function broadcastTagInfo(_tag, _port){
	var data = JSON.stringify([_tag]);
	// console.dir(_tag);
	// console.log(data.error);
	var server = _.findWhere(serverList, {port: _port});
	if(server != null){
		_.each(server.subscriber, function(_client){
	        _client.send(data);
		});
		if(_tag.Event == 'deleted'){
			console.log(('tag should be deleted if epc = ' + _tag.epc).error);
			server.tagList = _.filter(server.tagList, function(_tagtemp){
				return _tagtemp.epc != _tag.epc;
			});	
			console.dir(server.tagList);
		}else if(_tag.Event == 'added'){
			server.tagList = _.filter(server.tagList, function(_tagtemp){
				return _tagtemp.epc != _tag.epc;
			});	
			server.tagList.push(_tag);
		}
	}
}





