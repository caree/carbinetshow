

/******************************************

目标：
1. 标签读取敏感度
> 设置敏感度系数
> 可以用来调试标签在货架上是否处于合适的位置，并给出合适的建议
> 当标签被读取到的次数大于等于敏感度系数时，认为读写器读取标签性能比较好，建议减少信息接收次数法确定标签的存在
> 当标签被读取到的次数普遍少于敏感度系数时，认为读写器不能稳定高质量的读取到标签，建议通过改善硬件环境或者提高接收次数标准来确定标签的存在
eg.  tag: 123456789 low -----


2. 能够设定根据读取敏感度和持续读取到的次数来决策标签是否存在
> 读取程度：由于读写器功率的强弱不同以及标签的性质和位置的不同，每次读写器发送的读取到的标签信息中读取次数可能不同，这标志着读写器对该标签存在感认同标准的不同，读取到次数从1开始
> 读写器每隔一定时间（默认设置为2s）发送一次读取到的标签，去除误读的因素，可能需要1~3次才能确定标签存在，因此这种读取次数的设置不同，也决定了读取确定的时间不同
> 在接收标签下次数据和最近次数据之间的间隔时间，该间隔时间决定着是否应该认为标签已经不存在，或者两次接收数据为连续数据
eg.  tag: 123456789 too lag @@@@@@


--------------------------------------------
4. 支持获取标签API和事件推送
> 客户端根据API获取当前标签状态
> 标签发生变化时，将变化事件推送到客户端

*******************************************/ 
//部署参数
// var minSensitiveCount = 1;//敏感度下限次数，低于该值则说明标签读取不够灵敏
// var maxTagReadInterval = 6000;//标签上次读到和这次读到时间的最长间隔，超过该间隔认为标签离开了读写器可读取范围
// var minTagReadedConfirmCount = 2;//要确定标签是否已经读到，这是最少的次数

// 测试参数
var minSensitiveCount = 1;//敏感度下限次数，低于该值则说明标签读取不够灵敏
var maxTagReadInterval = 6000;//标签上次读到和这次读到时间的最长间隔，超过该间隔认为标签离开了读写器可读取范围
var minTagReadedConfirmCount = 2;//要确定标签是否已经读到，这是最少的次数
// {readerName:'r5000', port:5000, tagList:[], subscriber:[]}
// var readerPortNameMapList = [{port:5101, readerName:'reader1'}];

var dgram = require("dgram");
var _ = require("underscore");
var timeFormater = require('./timeFormat').getCurrentTime;
var wsServer = require('./wsServerInventory');

var ifPrintSensitiveReport = true;//是否打印敏感度报告

var server;//UDP Server
var guid =0;
var tagList=[];

var serverList = [];
//生成一个数组自身的阶差，例如 [15,9,7,6,1] 生成  [ 6, 2, 1, 5 ]
Array.prototype.mapMinusSelf = function(){
    if(this.length <= 1) return [];
    var mapMinus = [];
    for(var i = 0, len = this.length; i < len -1; i++){
        mapMinus.push(this[i] - this[i+1]);
    }

    return mapMinus;
}

_.each(readerPortNameMapList, function(_map){
	console.log('reader port   <====>   Name');
	console.log('       '+_map.port+'            '+_map.readerName);
});
console.log('********************************************************')

exports.startUDPListening = function(){
	_.each(readerPortNameMapList, function(_readerPortNameMap){
		var server = startServer(_readerPortNameMap.port);
		serverList.push({readerName: _readerPortNameMap.readerName, port:_readerPortNameMap.port, tagList:[], subscriber:[], udpServer:server});
	});

	// setInterval(testSync, 1000);
	setInterval(checkAllSeversTagExist, 2000);
};
exports.removeSubscriber = function(_ws){
	_.each(serverList, function(_server){
	    var index = _server.subscriber.indexOf(_ws);
	    if(index >= 0){
		    _server.subscriber.splice(index, 1);
	    }
	});
};
exports.addSubscriber = function(_ws, _readerList){
	//将ws客户端添加到每个它所订阅的读写器订阅者列表中
	_.each(_readerList, function(_readerName){
		console.log('subscriber readerName => ' + _readerName);
		console.dir(serverList);
		var server = _.find(serverList, function(_server){
			return _server.readerName == _readerName;
		});
		if(server != null){
			console.log('readerName => ' + server.readerName);
			var ws = _.find(server.subscriber, function(_subscriber){
				return _subscriber == _ws;
			});
			if(ws == null){
				server.subscriber.push(_ws);
				console.log('add subscriber => ' + _readerName);
			}
		}else{
			console.log('Can not find server ' + _readerName);
		}
	});
};
exports.getSubscribedExistTags = function(_ws){
	//根据ws客户端查找所有它所订阅的读写器中的标签
	var tags = _.map(serverList, function(_server){
		if(_.contains(_server.subscriber, _ws)){
			return getAllExistTags(_server.tagList);
		}
	});
	console.log('getSubscribedExistTags => ');
	var filteredTags = _.filter(tags, function(_tag){
		return _tag != null;
	});
	console.dir(filteredTags);
	return _.flatten(filteredTags);
};
exports.parseSingleItem = function(_item){
	return parseSingleItem(_item);
}

exports.parseMultiItem = function(_multiItem){
	return parseMultiItem(_multiItem);
}
exports.clearTempTagList = function(){
	tagList.length = 0;
}
exports.getAllExistTags = function(){
	return getAllExistTags();
}

//***********************************************


function getAllExistTags(_tagList){
	var uniqList = _.uniq(_tagList, false, function(_tag){
		return _tag.epc;
	});
	var existTagList = _.map(uniqList, function(_tag){
		return {epc: _tag.epc, antennaID: _tag.antennaID, Event: 'normal'};
	});
	return existTagList;
}
function broadcastTagInfo(_tags, _server){
	// var obj = {name:'nodes', content:msg};
	// wsServer.broadcastInfo(JSON.stringify(_tags));
	var data = JSON.stringify(_tags);
	_.each(_server.subscriber, function(_client){
    	// console.dir(_client);
        _client.send(data);
	});	
}
function checkAllSeversTagExist(){
	_.each(serverList, function(_server){
		checkTagExist(_server);
	});
}
function checkTagExist(_server){
	var tagList = _server.tagList
	var debugOutput = false;
	if(debugOutput == true){
		console.log('checkTagExist ...');
	}
	var currentTimeStamp = (new Date()).getTime();

    var groupedList = _.groupBy(tagList, function(_tag){
        return _tag.epc;
    });
    if(debugOutput == true){
	    console.dir(groupedList);
    }
    _.each(groupedList, function(_group, _keyEpc){
    	// console.log(_keyEpc + '=>');
    	// console.dir(_group);
        var sortedList = _.sortBy(_group, function(_tag){
            return -_tag.timeStamp;//保证按照从大到小的顺序排列
        });
    	// console.dir(sortedList);
        if(_.size(sortedList) >= minTagReadedConfirmCount){//至少要大于认定标签读取到的最少次数
        	//次数是标准的一方面，另一面看看这几次读取的时间是否连续
        	var timeStamps = _.map(sortedList, function(_tag){
        		return _tag.timeStamp;
        	});
        	timeStamps.unshift(currentTimeStamp);//插入当前时间，为生成阶差做准备
        	var mapMinus = timeStamps.mapMinusSelf();
        	// console.log(mapMinus);
        	var tooLargeResult = _.find(mapMinus, function(_result){
        		return _result > maxTagReadInterval;
        	});
        	console.log('tooLargeResult => ');
        	console.log(mapMinus);
        	console.log(tooLargeResult);
        	if(tooLargeResult != null){//说明有的标签读取的数据出现时间上的中断
        		//触发标签删除的事件
        		triggerTagEvent(_keyEpc, 'deleted', _server);
        		setTagExistState(_keyEpc, false, _server);

        	}else{//可以确定该标签已经读取到了
        		//触发标签添加的事件
        		triggerTagEvent(_keyEpc, 'added', _server);
        		setTagExistState(_keyEpc, true, _server);
        	}
        }
        // console.dir(sortedList);
    });
    //清除多余的过期数据，标准是 与当前的时间差大于 读取最少次数与最大延迟时间的乘积
    _server.tagList = _.filter(_server.tagList, function(_tag){
    	return (currentTimeStamp - _tag.timeStamp) < (minTagReadedConfirmCount * maxTagReadInterval);
    });
    // console.log('checkTagExist over ');
    printTagExistReport();
}

function startServer(_listeningPort){
	var server = dgram.createSocket("udp4");

	server.on("error", function (err) {
	  console.log("server error:\n" + err.stack);
	  server.close();
	});

	server.on("message", function (msg, rinfo) {
	  	console.log(timeFormater() + " : " + msg + " from " + rinfo.address + ":" + rinfo.port);
	  	// console.log(timeFormater() + " : " + msg + " from " + rinfo.address + ":" + rinfo.port);
		
		var tagList = parseMultiItem(String(msg), _listeningPort);
		// console.dir(tagList);
	});

	server.on("listening", function () {
	  var address = server.address();
	  console.log("server listening " +
	      address.address + ":" + address.port);
	});

	server.bind(_listeningPort);
	return server;
}
function setTagExistState(_epc, _state, _server){
	console.log('setTagExistState => '+ _epc + '  state => ' + _state);
	_.each(_server.tagList, function(_tag){
		if((_tag.epc == _epc) && (_tag.ifExist != _state)){
			_tag.setTagExistState(_state);
		}
	});
}

//针对单个的标签的变化通知客户端
function triggerTagEvent(_epc, _event, _server){
	var _ant = '00';
	var tag  = _.find(_server.tagList, function(_tag){
		return (_tag.epc == _epc);
	});
	// console.log('triggerTagEvent => ');
	// console.dir(tag);
	if(tag != null){
		console.log('_epc = ' + _epc + '  _event = ' + _event );
		if((_event == "added") && (tag.ifExist == false)){
			console.log(_epc + '  ' + _ant + ' +++++    ' + timeFormater());
			broadcastTagInfo([{epc: _epc, antennaID: _ant, Event: 'added'}], _server);
		}
		if( (tag.ifExist == true) && (_event == "deleted")){
			console.log(_epc + '  ' + _ant + ' -----    ' + timeFormater());
			broadcastTagInfo([{epc: _epc, antennaID: _ant, Event: 'deleted'}], _server);
		}
	}
}

function printTagExistReport(){
	console.log('printTagExistReport ...' + timeFormater());
	_.each(serverList, function(_server){
		console.log("reader => " + _server.readerName);
		var uniqList = _.uniq(_server.tagList, false, function(_tag){
			return _tag.epc;
		});
		_.each(uniqList, function(_tag){
			if(_tag.ifExist){
				console.log(_tag.epc + '  ' + _tag.antennaID + ' #####');
			}else{
				console.log(_tag.epc + ' ?????');
			}
		});
	});

	console.log('-------------------------');	
}
function Tag(_epc, _count, _antennaID){
	this.epc = _epc;
	this.antennaID = _antennaID;
	this.readCount = _count;
	this.timeStamp = (new Date()).getTime();
	this.ifExist = false;
}
Tag.prototype.setTagExistState = function(_state){
	this.ifExist = _state;
}
function parseMultiItem(_multiItem, _port){
	var items = _multiItem.match(/Disc:\d{4}\/\d{2}\/\d{2} \d{2}:\d{2}:\d{2}[,]\sLast:\d{4}\/\d{2}\/\d{2}\s\d{2}:\d{2}:\d{2}[,]\sCount:\d{5}[,]\sAnt:\d{2}[,]\sType:\d{2}[,]\sTag:[0-9A-F]{24}/g);
	// console.dir(items);
	_.each(items, function(_item){
		parseSingleItem(_item, _port);
	});
	return tagList;	
}

// like "Disc:2000/02/28 20:01:51, Last:2000/02/28 20:07:42, Count:00019, Ant:02, Type:04, Tag:300833B2DDD906C001010101 "
function parseSingleItem(_item, _port){
	var tagFlagIndex = _item.indexOf('Tag:');
	// console.log(tagFlagIndex);
	var countFlagIndex = _item.indexOf('Count:');
	var antFlagIndex = _item.indexOf('Ant:');

	var epc = _item.substr(tagFlagIndex + 4, 24);
	var antennaID = _item.substr(antFlagIndex+4, 2);
	var count = new Number(_item.substr(countFlagIndex+6, 5)).toFixed();
	var tag = new Tag(epc, count, antennaID);
	// console.log(tag);
	var bIfReadCountTooLittle = (tag.readCount < minSensitiveCount);//测试是否读取次数太少

	if(ifPrintSensitiveReport == true){
		if(bIfReadCountTooLittle == true){
			console.log('tag: ' + tag.epc + ' low -----');
		}
	}

	if(!bIfReadCountTooLittle){
		var server = _.find(serverList, function(_server){
			return _server.port == _port;
		});
		if(server != null){
			// console.log('add epc ' + tag.epc + ' to port '+_port);
			server.tagList.push(tag);
		}
		else{
			console.log('cannot find the server on port ' + _port);
			// console.dir(serverList);
		}
	}
	return tag;	
}





