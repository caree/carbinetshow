
var http = require('http');
var WebSocketServer = require('ws').Server;
var tagReadServerLogic = require('./tagReadServerLogic');
var _ = require("underscore");

var server;
var wss;
var clients = [];
listeningPort = 5100;// UDP端口

// ep.tail('tagAdded'+listeningPort, function(_tags){
// 	console.log(('port ' + listeningPort + ' new tag ').info);
// 	_.each(_tags, function(_tag){
// 		console.log(_tag.epc.info);
// 	});
// });
// ep.tail('tagDeleted'+listeningPort, function(_tags){
// 	console.log(('port ' + listeningPort + ' delete tag ').info);
// 	_.each(_tags, function(_tag){
// 		console.log(_tag.epc.info);
// 	});
// });

// var inventoryUnit = new InventoryUnit({listeningPort: 5100, ep: ep});
// inventoryUnit.startServer();

// startNewInventoryUnit({listeningPort: listeningPort, ep: ep});

tagReadServerLogic.startUDPListening();

exports.broadcastInfo = function(data){
	console.log('broadcastInfo => ');
	// console.dir(clients);
	_.each(clients, function(_client){
    	// console.dir(_client);
        _client.send(data);
	});	
}
exports.startWebSocketServer = function(app){

	server = http.createServer(app);
	wss = new WebSocketServer({server : server});
	// global.wss.broadcast = function(data) {
	// 	console.dir(clients);
	// 	_.each(clients, function(_client){
	//     	console.dir(_client);
	//         _client.send(data);
	// 	});
	// };
	wss.on('connection', function(ws){
		clients.push(ws);

	    console.log('new websocket connected'.info);
	    // 连接成功后将当前所有存在的标签都发送给客户端
	    // var tagList = InventoryLogic.getAllExistTags();
	    // var str = JSON.stringify(tagList);
	    // console.log(str);
	    // ws.send(str);

		ws.on('open', function(){
		    // console.log('connected');
		    // var obj = {name:'nodes', content:JSON.stringify(global.nodeList)};
		    // var str = JSON.stringify(obj);
		    // console.log(str);
		    // ws.send(str);
		  });
		
		ws.on('message', function(msg){
		    console.log('message => '+ msg);
		    var objMsg  = JSON.parse(msg);
		    // like { command: 'subscribe', content: '9601' };
		    // and like { command: 'alltags', content: '' }
		    if(objMsg.command == 'subscribe'){
		    	//将订阅读写器信息添加到系统中，其中添加的是读写器的命名
		    	var readerList = objMsg.content.split(',');
		    	console.log('subscribe readerList => '.info);
		    	console.dir(readerList);
		    	if(_.size(readerList) > 0){
			    	tagReadServerLogic.addSubscriber(ws, readerList);
		    	}
		    } 
		    if(objMsg.command == 'alltags'){
			    // 连接成功后将当前所有存在的标签都发送给客户端
			    var tagList = tagReadServerLogic.getSubscribedExistTags(ws);
			    var str = JSON.stringify(tagList);
			    console.log(('alltags => '+str).data);
			    ws.send(str);
		    }
		    // if(objMsg.command == 'alltags'){//接收到这个命令，将当前所有存在的标签都发送给客户端

		    // }
		    // wss.broadcast('broadcast => ' + msg);
		  });
		
		ws.on('close', function(){
		    var index = clients.indexOf(ws);
		    clients.splice(index, 1);
		    tagReadServerLogic.removeSubscriber(ws);
		    console.log('close =>'.warn);
		  });
		  
		});	
	
	return server;
}




