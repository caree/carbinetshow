

/******************************************

目标：
1. 标签读取敏感度
> 设置敏感度系数
> 可以用来调试标签在货架上是否处于合适的位置，并给出合适的建议
> 当标签被读取到的次数大于等于敏感度系数时，认为读写器读取标签性能比较好，建议减少信息接收次数法确定标签的存在
> 当标签被读取到的次数普遍少于敏感度系数时，认为读写器不能稳定高质量的读取到标签，建议通过改善硬件环境或者提高接收次数标准来确定标签的存在
eg.  tag: 123456789 low -----



2. 误读
>实时报告误读的标签，作为调节外在环境的参考
eg.  tag: 123456789 mis ******


3. 能够设定根据读取敏感度和持续读取到的次数来决策标签是否存在
> 读取程度：由于读写器功率的强弱不同以及标签的性质和位置的不同，每次读写器发送的读取到的标签信息中读取次数可能不同，这标志着读写器对该标签存在感认同标准的不同，读取到次数从1开始
> 读写器每隔一定时间（默认设置为2s）发送一次读取到的标签，去除误读的因素，可能需要1~3次才能确定标签存在，因此这种读取次数的设置不同，也决定了读取确定的时间不同
> 在接收标签下次数据和最近次数据之间的间隔时间，该间隔时间决定着是否应该认为标签已经不存在，或者两次接收数据为连续数据
eg.  tag: 123456789 too lag @@@@@@


--------------------------------------------
4. 支持获取标签API和事件推送
> 客户端根据API获取当前标签状态
> 标签发生变化时，将变化事件推送到客户端

*******************************************/ 
//部署参数
// var maxTagReadInterval = 4000;//标签上次读到和这次读到时间的最长间隔，超过该间隔认为标签离开了读写器可读取范围
// var minTagReadedConfirmCount = 2;//要确定标签是否已经读到，这是最少的次数
// var minSensitiveCount = 1;//敏感度下限次数，低于该值则说明标签读取不够灵敏

//测试参数
// var listeningPort = 5100;// UDP端口
var maxTagReadInterval = 4000;//标签上次读到和这次读到时间的最长间隔，超过该间隔认为标签离开了读写器可读取范围
var minTagReadedConfirmCount = 2;//要确定标签是否已经读到，这是最少的次数
var minSensitiveCount = 1;//敏感度下限次数，低于该值则说明标签读取不够灵敏


var dgram = require("dgram");
var _ = require("underscore");
var timeFormater = require('./timeFormat').getCurrentTime;
var dataParser = require('./dataParser');

var ifPrintMisReading = true;//是否打印误读信息
var antennaIDScrope = ['01', '02', '04', '08'];//正确天线值，如果天线值为该数组中的一个，说明没有误读
var ifPrintSensitiveReport = true;//是否打印敏感度报告

var server;//UDP Server
var tagList = dataParser.tagList;
var lastExistsTagList = null;
//生成一个数组自身的阶差，例如 [15,9,7,6,1] 生成  [ 6, 2, 1, 5 ]
Array.prototype.mapMinusSelf = function(){
    if(this.length <= 1) return [];
    var mapMinus = [];
    for(var i = 0, len = this.length; i < len -1; i++){
        mapMinus.push(this[i] - this[i+1]);
    }
    return mapMinus;
}

exports.getAllExistTags = function(){
	return getAllExistTags();
}

dataParser.startUDPListening();
setInterval(checkTagExist, 1000);

function getAllExistTags(){
	var uniqList = _.uniq(tagList, false, function(_tag){
		return _tag.epc;
	});
	var existTagList = _.map(uniqList, function(_tag){
		return {epc: _tag.epc, antennaID: _tag.antennaID, Event: 'normal'};
	});
	return existTagList;
}

function broadcastTagInfo(_tags){
	// var obj = {name:'nodes', content:msg};
	global.wss.broadcast(JSON.stringify(_tags));
}
function checkTagExist(){
	console.log('checkTagExist ...');

	var currentExistsTagList = getCurrentExistsTagList();
	//与上次的列表进行比较，可以发现变化的标签
	// 切换天线的判断以及新出现和消失
	if(lastExistsTagList == null){
		//第一次
		ep.emit('tagAdded', _.map(currentExistsTagList, function(_tag){
			_tag.Event = "added";			
			return _tag;
		});
	}else{
		seeWhichTagChanged(currentExistsTagList, lastExistsTagList)
	}	

	lastExistsTagList = currentExistsTagList;
	return;
}
function seeWhichTagChanged(_currentExistsEpcs, _lastExistsTagList){
	// 综合比较
	var lastExistsEpcs = _.pluck(_lastExistsTagList, "epc");
	var _currentExistsEpcs = _.pluck(currentExistsTagList, "epc");
	var epcsDisappear = _.difference(lastExistsEpcs, _currentExistsEpcs);
	var tagsDisappear = _.without(_.map(_lastExistsTagList, function(_tag){
		if(_.contains(epcsDisappear, _tag.epc)){
			_tag.Event = "deleted";
			return _tag;
		}else{
			return null;
		}
	}), null);
	ep.emit('tagDeleted', tagsDisappear);

	var epcsAppear = _.difference(_currentExistsEpcs, lastExistsEpcs);
	var tagsAppear = _.without(_.map(currentExistsTagList, function(_tag){
		if(_.contains(epcsAppear, _tag.epc)){
			_tag.Event = "added";			
			return _tag;
		}else{
			return null;
		}
	}) ,null);
	ep.emit('tagAdded', tagsAppear);
	
	//两次都存在，看是否天线有变化
	var epcsAlwaysExists = _.intersection(_currentExistsEpcs, lastExistsEpcs);
	var tagsAntenaChanged = _.without(_.map(epcsAlwaysExists, function(_epc){
		var lastTag = _.find(_lastExistsTagList, {epc: _epc});
		var currentTag = _.find(currentExistsTagList, {epc: _epc});
		if(lastTag.antennaID != currentTag.antennaID) return currentTag;
		return null;
	}), null);
	if(_.size(tagsAntenaChanged) > 0){
		ep.emit('tagChanged', tagsAntenaChanged);
	}

}
function getCurrentExistsTagList(){
	var currentTimeStamp = (new Date()).getTime();
	var currentExistsTagList = _.without(_.map(tagList, function(_tag){
		var records = _tag.records;
		// 至少有多次的读取记录才能参与是否真正读取的选择
		if(_.size(records) <= minTagReadedConfirmCount){
			console.info(_tag.epc + ': no enough records');
			return null;
		} 
		
		if(_.every(_.first(records, minTagReadedConfirmCount), function(_record){
			//todo: 以及偶尔误读的判断

			//排除误读，最近几次都不能出现误读,排除读取次数太少的标签
			if(_.contains(antennaIDScrope ,_record.antennaID) == false) return false
			if(_record.readCount < minSensitiveCount) return false;

			//最后通过读取的间隔时间确定

		}) == false){
			console.info(_tag.epc + ': ')
			return null;
		}

		// 获取时间序列，评判时间间隔
		var timeStamps = _.pluck(records, 'timeStamp');
    	timeStamps.unshift(currentTimeStamp);//插入当前时间，为生成阶差做准备
    	var mapMinus = timeStamps.mapMinusSelf();
    	if(_.every(mapMinus, function(_minusResult){
    		return _minusResult <= maxTagReadInterval;
	    	}) == false){
    		console.info(_tag.epc + ':  time interval too large')
    		return null;
    	} 

		return {epc: _tag.epc, antennaID: _tag.records[0].antennaID};
	}), null);
	return currentExistsTagList;
}
function setTagExistState(_epc, _ant, _state){
	_.each(tagList, function(_tag){
		if(_tag.epc == _epc){
			_tag.setTagExistState(_state);
		}
	});
}
function triggerTagEvent(_epc, _ant, _event){
	var tag  = _.find(tagList, function(_tag){
		return (_tag.epc == _epc) && (_tag.antennaID == _ant);
	});
	console.log('triggerTagEvent => ');
	// console.dir(tag);
	if(tag != null){
		console.log('_epc = ' + _epc + '  _event = ' + _event + '   Exist state = ' + tag.ifExist);
		if((_event == "added") && (tag.ifExist == false)){
			console.log(_epc + '  ' + _ant + ' +++++    ' + timeFormater());
			broadcastTagInfo([{epc: _epc, antennaID: _ant, Event: 'added'}]);
		}
		if( (tag.ifExist == true) && (_event == "deleted")){
			console.log(_epc + '  ' + _ant + ' -----    ' + timeFormater());
			broadcastTagInfo([{epc: _epc, antennaID: _ant, Event: 'deleted'}]);
		}
	}

}

function printTagExistReport(){
	console.log('printTagExistReport ...' + timeFormater());

	var uniqList = _.uniq(tagList, false, function(_tag){
		return _tag.epc;
	});
	_.each(uniqList, function(_tag){
		if(_tag.ifExist){
			console.log(_tag.epc + '  ' + _tag.antennaID + ' !!!!!');
		}else{
			console.log(_tag.epc + ' ?????');
		}
	});
	console.log('-------------------------');	
}


    // var groupedList = _.groupBy(tagList, function(_tag){
    //     return _tag.epc + _tag.antennaID;
    // });

    // console.dir(groupedList);
    // _.each(groupedList, function(_group, _key){
    // 	// console.log(_keyEpc + '=>');
    // 	// console.dir(_group);
    // 	var _keyEpc = _key.substr(0, 24);
    // 	var _ant = _key.substr(24, 2);
    //     var sortedList = _.sortBy(_group, function(_tag){
    //         return -_tag.timeStamp;//保证按照从大到小的顺序排列
    //     });
    // 	// console.dir(sortedList);
    //     if(_.size(sortedList) >= minTagReadedConfirmCount){//至少要大于认定标签读取到的最少次数
    //     	//次数是标准的一方面，另一面看看这几次读取的时间是否连续
    //     	var timeStamps = _.map(sortedList, function(_tag){
    //     		return _tag.timeStamp;
    //     	});
    //     	timeStamps.unshift(currentTimeStamp);//插入当前时间，为生成阶差做准备
    //     	var mapMinus = timeStamps.mapMinusSelf();
    //     	// console.log(mapMinus);
    //     	var tooLargeResult = _.find(mapMinus, function(_result){
    //     		return _result > maxTagReadInterval;
    //     	});
    //     	console.log('tooLargeResult => '+ _keyEpc);
    //     	console.log(mapMinus);
    //     	console.log(tooLargeResult);
    //     	if(tooLargeResult != null){//说明有的标签读取的数据出现时间上的中断
    //     		//触发标签删除的事件
    //     		triggerTagEvent(_keyEpc, _ant, 'deleted');

    //     		setTagExistState(_keyEpc, _ant, false);

    //     	}else{//可以确定该标签已经读取到了
    //     		//触发标签添加的事件
    //     		triggerTagEvent(_keyEpc, _ant, 'added');
    //     		setTagExistState(_keyEpc, _ant, true);

    //     	}

    //     }
    //     // console.dir(sortedList);
    // });
    // //清除多余的过期数据，标准是 与当前的时间差大于 读取最少次数与最大延迟时间的乘积
    // tagList = _.filter(tagList, function(_tag){
    // 	return (currentTimeStamp - _tag.timeStamp) < (minTagReadedConfirmCount * maxTagReadInterval);
    // });




