var WebSocket = require('faye-websocket');
var _  = require('underscore');

var eventNames = ["epcServer:newTypeInfo", 'epcServer:deleteTypeInfo', 'epcServer:newProduct', 'epcServer:deleteProduct'];
//定义一个公共变量
// WebSocketEventCenter = null;

__createEventCenterWebSocket();

setInterval(function(){
	if(WebSocketEventCenter == null){
		__createEventCenterWebSocket();
	}
}, 30 * 1000);



function __onMessage(_msg){
	// console.dir(_msg);
	var cmds = JSON.parse(_msg);
	console.log('receive cmd => '.info);
	console.dir(cmds);
	_.each(cmds, function(cmd){
		switch(cmd.name){
			case 'epcServer:newTypeInfo':
				g_EventProxy.emit('ProductTypeChanged');
			break;
			case 'epcServer:deleteTypeInfo':
				g_EventProxy.emit('ProductTypeChanged');
			break;		
			case 'epcServer:newProduct':
				g_EventProxy.emit('ProductChanged');
			break;		
			case 'epcServer:deleteProduct':
				g_EventProxy.emit('ProductChanged');
			break;			
		}		
	})
}
function __createEventCenterWebSocket() {
    console.log(('__createEventCenterWebSocket =>').data);
    WebSocketEventCenter = new WebSocket.Client(wsUriEventCenter);

    WebSocketEventCenter.on('open', function (evt) { 
	    console.log(('WebSocketEventCenter open!').info);
	    var cmds = _.map(eventNames, function(_name){
	    	return {name: _name, msgType: 'reg'};
	    })
	    _.each(cmds, function(_cmd){
	    	WebSocketEventCenter.send(JSON.stringify(_cmd));
	    });

    });

    WebSocketEventCenter.on('close', function (evt) { 
	    console.log(('WebSocketEventCenter closed!').info)
	    WebSocketEventCenter = null;    	
    });

    WebSocketEventCenter.on('message', function (evt) 
    { 
        __onMessage(evt.data); 
    });

    WebSocketEventCenter.on('error', function (evt) { 
	    console.log(('WebSocketEventCenter error!!!').error);
	    WebSocketEventCenter = null;
    });
}


