
var http = require('http');
var WebSocketServer = require('ws').Server;
// var InventoryUnit = require('./InventoryLogicUnit');
var InventoryLogic = require('./InventoryLogic');
var _ = require("underscore");

require('./communication');

var server;
var wss;
var clients = [];
listeningPort = 5100;// UDP端口


InventoryLogic.startUDPListening();



g_EventProxy.tail('broadcastInventory', broadcastInfo);

function broadcastInfo(_data){
	console.log(('broadcastInfo => ' + _data).data);
	_.each(clients, function(_client){
        _client.send(_data);
	});	
}
exports.startWebSocketServer = function(app){

	server = http.createServer(app);
	wss = new WebSocketServer({server : server});

	wss.on('connection', function(ws){
		clients.push(ws);

	    console.log(('new websocket connected').error);
		var records = InventoryLogic.getCurrentInventories();
		// console.log('new ws open '.error);
		console.dir(records);
		ws.send(JSON.stringify(records));
		// _.each(records, function(_record){
		// 	ws.send(JSON.stringify(_record));
		// });

		ws.on('open', function(){

		  });
		
		ws.on('message', function(msg){
		    console.log(('message => '+ msg).data);
		    var objMsg  = JSON.parse(msg);

		  });
		
		ws.on('close', function(){
		    var index = clients.indexOf(ws);
		    clients.splice(index, 1);
		    // InventoryLogic.removeSubscriber(ws);
		    console.log('close =>'.info);
		  });
		  
		});	
	
	return server;
}




