
// {readerName:'r5000', port:5000, tagList:[], subscriber:[]}
var WebSocket = require('faye-websocket');

var request = require('request');
var _ = require("underscore");
var timeFormater = require('./timeFormat').getCurrentTime;
var InventoryUnit = require('./InventoryLogicUnit');
var EventProxy = require('eventproxy');
var logicEP = new EventProxy();

var serverList = [];
// var epcTypeMapList = [];
// var typeInfoList = [];
var readTagList = [];//储存读取到的标签，但是只包含有产品类型匹配的部分
var inventoryRecords = [];
// var WebSocketEventCenter = null;
var allReadRawTags = [];//记录所有读到的标签，不做过滤

//更新产品EPC列表和产品类型列表
g_EventProxy.tail('ProductChanged', __refreshProductInfo);
g_EventProxy.tail('ProductTypeChanged', __refreshProductTypeInfo);


_.each(readerPortNameMapList, function(_map){
	console.log('reader port   <====>   Name'.info);
	console.log(('       '+_map.port+'            '+_map.readerName).info);
});
console.log('********************************************************'.data);

requestBaseData();
require('./communication');

setInterval(__startInventoryAlert, IntervalInventoryAlert);
// setInterval(__startInventoryAlert, 5000);

//***************************************************************************

exports.checkInventory = function(){
	__checkInventory();
}

exports.getCurrentInventories = function(){
	return inventoryRecords;
}
exports.startUDPListening = function(){
	_.each(readerPortNameMapList, function(_readerPortNameMap){
		// var server = startServer(_readerPortNameMap.port);
		__startNewInventoryUnit(
			{listeningPort: _readerPortNameMap.port
				, ep: g_EventProxy
				, minTagReadedConfirmCount: _readerPortNameMap.minTagReadedConfirmCount || defaultMinTagReadedConfirmCount
				, maxTagReadInterval: _readerPortNameMap.maxTagReadInterval || defaultmaxTagReadInterval});

		serverList.push({readerName: _readerPortNameMap.readerName, port:_readerPortNameMap.port, tagList:[], subscriber:[]});
	});
};
function __startInventoryAlert(){
	var alertInventories = _.without(
		_.map(minInventorySettings, function(_setting){
		var inventoryCrt = _.findWhere(inventoryRecords, {productCode: _setting.productCode});
		if(inventoryCrt != null && _setting.value > inventoryCrt.value){
			return {productName: inventoryCrt.productName, value: (_setting.value - inventoryCrt.value)};
		}else return {productName: _setting.productName, value: (_setting.value)};
	}), null);
	console.log('inventory alert => '.info);
	console.dir(alertInventories);
	if(_.size(alertInventories) <= 0) return;
	var cmdStr = _.reduce(alertInventories, function(_str, _alertInventory){
		return _str + _alertInventory.productName + '缺货数量 ' + _alertInventory.value + ' ';
	}, '');

    var cmd = {name:'led', msgType:'push', para: cmdStr};
    var str = (JSON.stringify(cmd));
    console.log(('command => ' + str).info);
    if(WebSocketEventCenter != null){
	    WebSocketEventCenter.send(str);
    }

}
function __lessInventory_handler(_obj){
	//库存少于最低库存时的处理, value表示缺货数量
	// {itemType: "01", value: 1}
	var typeInfo = _.findWhere(productTypeInfoList, {productCode: _obj.itemType});
	if(typeInfo != null){
	    var cmd = {name:'led', msgType:'push', para: typeInfo.typeName + '缺货数量 ' + _obj.value};
	    var str = (JSON.stringify(cmd));
	    console.log(('command => ' + str).info);
	    if(WebSocketEventCenter != null){
		    WebSocketEventCenter.send(str);
	    }
	}	
}

//??? 通知订阅的终端各个产品的数量
function __broadcastInventory(_productCode, _currentValue){
	var record = _.findWhere(inventoryRecords, {productCode: _productCode});
	if(record == null){
		var typeName = null;
		var typeInfo = _.findWhere(productTypeInfoList, {productCode: _productCode});
		if(typeInfo != null){
			typeName = typeInfo.productName;
		}else{
			console.log('there is no such type, thers is some error'.error);
			return;
		}	
		var obj = {productCode: _productCode, productName: typeName, value: _currentValue};
		inventoryRecords.push(obj);
		g_EventProxy.emit('broadcastInventory', JSON.stringify(obj));
	}else if(record.value != _currentValue){
		record.value = _currentValue;
		g_EventProxy.emit('broadcastInventory', JSON.stringify(record));
	} 	
}
function requestBaseData(){
	console.log('requestBaseData ...'.data);
	__refreshProductInfo();
	__refreshProductTypeInfo();
}
function __refreshProductTypeInfo(){
	httpRequestPost('http://'+ EpcServerIP +':6010/listTypeInfo', {}).then(function(_response){
		// console.dir(_response);
		// console.log('*****************************')
		// console.dir(_response[0]);
		// console.log('*****************************')
		var data = _response[0].body;
		// console.log(data);
		try{
			var list = JSON.parse(data);
			productTypeInfoList = list;
			console.log('productTypeInfoList updated!!'.info);
			console.dir(productTypeInfoList);	
			// var CountFirst = _.size(inventoryRecords);
			// var inventoryRecordsGroup1 = _.groupBy(inventoryRecords, function(_record){
			// 	return _record.productCode;
			// });
			// var keys1 = _.keys(inventoryRecordsGroup1);
			//产品类型发生变化，刷新产品类型的定义
			// inventoryRecords = _.map(inventoryRecords, function(_record){
			// 	var typeInfo = _.findWhere(productTypeInfoList, {productCode: _record.productCode});
			// 	if(typeInfo != null){
			// 		_record.productName = typeInfo.productName;
			// 	}else{
			// 		return _record.bChangedFlag = true;
			// 	}					
			// 	return _record;
			// });
			// var changedRecords = _.where(inventoryRecords, {bChangedFlag: true});
			// inventoryRecords = _.reject(inventoryRecords, function(_record){
			// 	return _record.bChangedFlag == true;
			// });
			// inventoryRecords = [];
			__checkInventory();
			__refreshMinInventorySetting();
			//通知页面刷新
			// var inventoryRecordsGroup2 = _.groupBy(inventoryRecords, function(_record){
			// 	return _record.productCode;
			// });
			// var keys2 = _.keys(inventoryRecordsGroup2);

			// if(_.size(_.difference(keys1, keys2)) > 0){
			// 	//产品类型变化了
			// }
			// if(_.size(changedRecords) > 0){
			// 	_.each(changedRecords, function(_record){

			// 	})
			// }

		}catch(e){
			console.log(data.error);
		}
	}).catch(function(error){
		console.log('error <= requestBaseData listTypeInfo'.error);
	});
}
function __refreshProductInfo(){
	httpRequestPost('http://'+ EpcServerIP +':6010/productlist', {}).then(function(_response){
		var data = _response[0].body;
		// console.log(data);
		try{
			var list = JSON.parse(data);
			productEPClist = list;
			console.dir(productEPClist);
			__checkInventory();
			//鉴定已经确定读取的标签
			//检查是否EPC所属的产品类型发生了变化
			// var ifItemTypeChanged = false;
			// _.each(readTagList, function(_tag){
			// 	var type = _.findWhere(productEPClist, {productEPC: _tag.productEPC});
			// 	if(type != null){
			// 		_tag.productCode = type.productCode;
			// 		ifItemTypeChanged = true;
			// 	}else{
			// 		_tag.productCode = null;
			// 		ifItemTypeChanged = true;
			// 	}
			// });
			// if(ifItemTypeChanged == true){
			// 	__checkInventory();
			// }

		}catch(e){
			console.log(data.error);
		}
	}).catch(function(error){
		console.log('error <= requestBaseData productlist'.error);
	});	
}
function __checkInventory(){
	// console.log('__checkInventory ...'.warn);
	// console.dir(readTagList);
	// _.each(minInventorySettings, function(_setting){
	// 	var tags = _.filter(readTagList, function(_tag){
	// 		return _tag.productCode == _setting.productCode;
	// 	});
	// 	var currentCount = _.size(tags);
	// 	// console.log(_setting.productCode + ' has ' + currentCount + ' tags');
	// 	if(currentCount < _setting.value){
	// 		var gap = _setting.value - currentCount;
	// 		console.log((_setting.productCode + ' need inventory ').info + (''+gap).error);
	// 		__lessInventory_handler({productCode: _setting.productCode, value: gap});
	// 		// logicEP.emit('lessInventory', {productCode: _setting.productCode, value: gap});
	// 	}else{
	// 		console.log((_setting.productCode + ' inventory ok').data);
	// 	}
	// 	__broadcastInventory(_setting.productCode, currentCount);
	// });	
	readTagList = [];
	_.each(allReadRawTags, function(_tag){
		var findedTag = _.findWhere(readTagList, {productEPC: _tag.epc});
		if(findedTag == null){
			// console.dir(productEPClist);
			var type = _.findWhere(productEPClist, {productEPC: _tag.epc});
			if(type != null){
				_tag.productCode = type.productCode;
				readTagList.push({productEPC: _tag.epc, productCode: _tag.productCode, antenaID: _tag.antenaID, Event: _tag.Event});
			}				
		} 
	})

	inventoryRecords = [];
	// 将读到的标签按照类型分类
	var groups = _.groupBy(readTagList, function(_tag){
		return _tag.productCode;
	});
	var keys = _.keys(groups);
	_.each(keys, function(_productCode){
		var tags = groups[_productCode];
		var _currentValue = _.size(tags);
		var record = _.findWhere(inventoryRecords, {productCode: _productCode});
		if(record == null){
			var typeName = null;
			var typeInfo = _.findWhere(productTypeInfoList, {productCode: _productCode});
			if(typeInfo != null){
				typeName = typeInfo.productName;
			}else{//如果没有该
				console.log('there is no such type, thers is some error'.error);
				return;
			}	
			var obj = {productCode: _productCode, productName: typeName, value: _currentValue};
			inventoryRecords.push(obj);
			// g_EventProxy.emit('broadcastInventory', JSON.stringify(obj));
		}else if(record.value != _currentValue){
			record.value = _currentValue;
			// g_EventProxy.emit('broadcastInventory', JSON.stringify(record));
		} 			
	})
	g_EventProxy.emit('broadcastInventory', JSON.stringify(inventoryRecords));

}
function __refreshMinInventorySetting(){
	minInventorySettings = _.filter(minInventorySettingConfig, function(_setting){
		return _.findWhere(productTypeInfoList, {productCode: _setting.productCode}) != null;
	});
	minInventorySettings = _.map(minInventorySettings, function(_setting){
		var productInfo = _.findWhere(productTypeInfoList, {productCode: _setting.productCode});
		if(productInfo != null){
			_setting.productName = productInfo.productName;
		}else{
			_setting.productName = "未知";
		}
		return _setting;
	})
	console.log('refreshed minInventorySettings =>'.info);
	console.dir(minInventorySettings);
}
function __startNewInventoryUnit(_paras){
	var listeningPort = _paras.listeningPort || 10000;
	var ep = _paras.ep || null;
	ep.tail('tagAdded'+listeningPort, function(_tags){
		// [ { epc: '3005FB63AC1F3681EC880468', antenaID: '01', Event: 'added' } ]
		console.log(('port ' + listeningPort + ' new tag ').info);

		// _.each(_tags, function(_tag){
		// 	// broadcastTagInfo({epc: _tag.epc, antenaID: _tag.antenaID, Event: 'added'}, listeningPort);
		// 	console.log(_tag.epc.info);
		// });

		console.log('tagAdded => _tags'.data);
		console.dir(_tags);
		_.each(_tags, function(_tag){
			// if(!_.contains(allReadRawTags, _tag.epc)){
			// 	allReadRawTags.push(_tag.epc);
			// }
			if(_.findWhere(allReadRawTags, function(_readTag){
				return _tag.epc == _readTag.epc;
			}) == null){
				allReadRawTags.push(_tag);
			}
		});
		// _.each(_tags, function(_tag){
		// 	var findedTag = _.findWhere(readTagList, {productEPC: _tag.epc});
		// 	if(findedTag == null){
		// 		// console.dir(productEPClist);
		// 		var type = _.findWhere(productEPClist, {productEPC: _tag.epc});
		// 		if(type != null){
		// 			_tag.productCode = type.productCode;
		// 		}				
		// 		readTagList.push({productEPC: _tag.epc, productCode: _tag.productCode, antenaID: _tag.antenaID, Event: _tag.Event});
		// 	} 
		// })
		__checkInventory();

	});
	ep.tail('tagDeleted'+listeningPort, function(_tags){
		console.log(('port ' + listeningPort + ' delete tag ').info);
		// _.each(_tags, function(_tag){
		// 	console.log(_tag.epc.info);
		// });
		// readTagList = _.filter(readTagList, function(_tag){
		// 	return _.findWhere(_tags, {epc: _tag.productEPC}) == null;
		// });
		// logicEP.emit('checkInventory');
		allReadRawTags = _.filter(allReadRawTags, function(_tag){
			return _.findWhere(_tags, {epc: _tag.epc}) == null;
		});
		__checkInventory();	
	
	});

	var inventoryUnit = new InventoryUnit(_paras);
	// var inventoryUnit = new InventoryUnit({listeningPort: listeningPort, ep: ep});
	inventoryUnit.startServer();
}







