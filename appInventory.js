

require('./routes/axonPortConfig');
require('./minInventorySettingConfig');

IntervalRequestEpcInfo = 60 * 1000;
IntervalCheckInventory = 30 * 1000;
IntervalInventoryAlert = 10 * 60 * 1000;
EpcServerIP = "127.0.0.1";
// EpcServerIP = "192.168.48.121";

productTypeInfoList = [];
productEPClist = [];
minInventorySettings = [];
wsUriEventCenter = "ws://127.0.0.1:6013";
WebSocketEventCenter = null;

defaultMinTagReadedConfirmCount = 2;
defaultmaxTagReadInterval = 1000 * 10;

readerPortNameMapList = 
[
    {
        port:5100
        , readerName:'reader1'
        , minTagReadedConfirmCount: 2
        , maxTagReadInterval: 1000 * 15}, 
    
    {
        port:5210
        , minTagReadedConfirmCount: 2
        , maxTagReadInterval: 1000 * 10
        , readerName:'reader2'}

];//

      
/**
 * Module dependencies.
 */
var colors = require('colors');

colors.setTheme({
  silly: 'rainbow',
  input: 'grey',
  verbose: 'cyan',
  prompt: 'grey',
  info: 'green',
  data: 'grey',
  help: 'cyan',
  warn: 'yellow',
  debug: 'blue',
  error: 'red'
});
var EventProxy = require('eventproxy');
g_EventProxy = new EventProxy();

var Q        = require('q');

var request = require('request');
httpRequestGet = Q.denodeify(request.get);
httpRequestPost = Q.denodeify(request.post);


var express = require('express');
var routes = require('./routes');
// var user = require('./routes/user');
var http = require('http');
var path = require('path');
var wsServerInventory = require('./routes/wsServerInventory');

var app = express();

var server = wsServerInventory.startWebSocketServer(app);
// all environments
app.set('port', process.env.PORT || 6001);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

// app.get('/carbinet2', routes.carbinet2);
app.get('/carbinet', routes.carbinet);
app.get('/1', routes.carbinet);
app.get('/2', routes.inventoryAlert);
app.get('/inventoryAlert', routes.inventoryAlert);
app.get('/', routes.inventoryAlert);

// app.get('/users', user.list);

server.listen(app.get('port'), function(){
  console.log(('Inventory Carbinet server listening on port ' + app.get('port')).info);
});
