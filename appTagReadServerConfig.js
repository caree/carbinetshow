
exports.readerPortNameMapList = function(){
  return readerPortNameMapList;
};
exports.simpleTagReadPortNameList = function(){
  return simpleTagReadPortNameList;
};

readerPortNameMapList = 
[
    {
        port:5100
        , readerName:'reader1'
        , minTagReadedConfirmCount: 1
        , maxTagReadInterval: 1000 * 4}, 
    
    {
        port:5210
        , minTagReadedConfirmCount: 1
        , maxTagReadInterval: 1000 * 4
        , readerName:'reader2'}

];//

//如果基于不同的端口，那么作为不同的读写器使用；如果在同一个端口，则基于天线的不同使用
//自定义协议 [flag, epc]
// like [5301, 3005FB63AC1F3841EC880468]
simpleTagReadPortNameList = 
[
    {
      port: 5301
      , minTagReadedConfirmCount: 1
      , maxTagReadInterval: 1000 * 1
      , readerName: 'simpleReader1'
    }
];

