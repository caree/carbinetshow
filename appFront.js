
//部署参数
// EpcServerIP = "192.168.48.121";
// listeningPort = 5100;// UDP端口
//测试参数
EpcServerIP = "192.168.48.121";
listeningPort = 5100;// UDP端口
wsUriEventCenter = "ws://192.168.48.121:6013";
// wsUriEventCenter = "ws://127.0.0.1:6013";

listeningPort = 5100;// UDP端口

/**
 * **************************************************
 */
var EventProxy = require('eventproxy');
ep = new EventProxy();
var colors = require('colors');

colors.setTheme({
  silly: 'rainbow',
  input: 'grey',
  verbose: 'cyan',
  prompt: 'grey',
  info: 'green',
  data: 'grey',
  help: 'cyan',
  warn: 'yellow',
  debug: 'blue',
  error: 'red'
});

// require('./routes/simulator');
var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var path = require('path');
var wsServer = require('./routes/wsServerFront');

var app = express();

var server = wsServer.startWebSocketServer(app);
// udpServer.startUDPListening();
// all environments
app.set('port', process.env.PORT || 6002);
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

// app.get('/signin', routes.signin);
// app.get('/theme', routes.theme);
app.get('/carbinetFront', routes.carbinetFront4);
app.get('/', routes.carbinetFront4);
// app.get('/carbinetFront2', routes.carbinetFront2);
// app.get('/carbinetFront3', routes.carbinetFront3);
// app.get('/carbinetFront4', routes.carbinetFront);
// app.get('/carbinetFront5', routes.carbinetFront5);
// app.get('/carbinet2', routes.carbinet2);
// app.get('/carbinet3', routes.carbinet3);
// app.get('/wsTest', routes.wsTest);

// app.get('/users', user.list);

server.listen(app.get('port'), function(){
  console.log(('Carbinet server listening on port ' + app.get('port')).info);
});
