require("should");
var _ = require("underscore");


describe(" => ", function() {
    it('array test => ', function(){
        var content = '9601,9602,9603';
        var list = content.split(',');
        (_.size(list) == 3).should.be.true;
        (_.contains(list, '9603')).should.be.true;
    });
    it('', function(){
        var tagList=[[{antena:1}, {antena:2}],[{antena:3}, {antena:4}]];
        var newTagList = _.flatten(tagList);
        console.dir(newTagList);
    });

    it('', function(){
        var strSrc = "one two one three";
        console.log(strSrc);

        var strDst = strSrc.replace(/(one)/g, function(match, pro){
            console.log('pro => ' + pro);
            console.log('match => ' + match);
            return 'ten';
        });
        console.log(strDst);

        setup('1.:method 2.:url');

    });

    it('test difference', function(){
        console.info('test difference');
        var array1 = [{epc: '001', antena: '01'}, {epc: '002', antena: '02'}];
        var array2 = [{epc: '001', antena: '01'}, {epc: '002', antena: '03'}];
        var difference = _.difference(array1, array2);
        console.dir(difference);
    });
});

function setup(format) {
    var req = {method: 'method1', url: 'url1', header: 'header1'};
    // var regexp = /:\w+/g;
    var regexp = /:(\w+)/g;
    var str = format.replace(regexp, function(match, property, arg3, arg4){
        console.log('match => ' + match + '  property => ' + property);
        console.log('arg3 => ' + arg3 + ' arg4 => ' + arg4);
        return req[property];
    });
    console.log(str);
}


